from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Hat, LocationVO
from common.json import ModelEncoder

# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "id",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }
    # def get_extra_data(self, o):
    #     return {"location": o.location.closet_name}


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse (
            {"hats": hats},
            encoder = HatListEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            # location_href = f'/api/locations/{content["location"]}/'
            location_href = content["location"]
            print(location_href, "!!!!!!")

            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid location id"},
                status = 400
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder = HatDetailEncoder,
            safe=False,
        )

@require_http_methods (["GET", "DELETE"])
def api_hat_details(request, id):
    if request.method == "GET":
        try:
            hats = Hat.objects.get(id=id)
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "This hat doesn't exist!"},
                status = 400
            )
    else:
        # try:
        #     hats = Hat.objects.get(id=id)
        #     hats.delete()
        #     return JsonResponse(
        #         hats,
        #         encoder = HatDetailEncoder,
        #         safe=False,
        #     )
        # except Hat.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "This hat doesn't exist"},
        #         status = 400
        #     )
        count, _=Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0 })
