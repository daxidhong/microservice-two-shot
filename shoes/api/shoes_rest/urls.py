from django.urls import path
from .views import list_shoes, shoe_description


urlpatterns = [
    path("shoes/", list_shoes, name="shoe_list"),
    path("shoes/<int:pk>/", shoe_description, name="shoe_description")
]
