from django.db import models

# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href= models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.closet_name

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=40, null=True)
    name = models.CharField(max_length=40, null=True)
    color = models.CharField(max_length=40, null=True)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
