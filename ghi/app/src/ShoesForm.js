import React, { useEffect , useState } from 'react'

export default function ShoesForm(){
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [bin, setBin] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }
    const handlePictureChange= (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    useEffect(() => {
        async function getBins() {
            const url = "http://localhost:8100/api/bins/"

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);
            }
        }
        getBins();
    }, []);


    async function handleSubmit(event) {
        event.preventDefault();
        const data = {};

        data.manufacturer = manufacturer;
        data.name = name;
        data.color = color;
        data.bin = bin
        data.picture_url = pictureUrl;

        const shoesUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            setManufacturer('');
            setName('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }
    }

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a New Shoe</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"></input>
                <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={pictureUrl} placeholder="picture" required type="url" name="picture" id="picture" className="form-control"></input>
                <label htmlFor="color">Picture Url</label>
                </div>
                <div className="mb-3">
                <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a Bin</option>
                    {bins.map(bin => {
                            return (
                            <option key={bin.href} value={bin.id}>
                                {bin.closet_name}
                            </option>
                            );
                        })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    </>
    )

}
