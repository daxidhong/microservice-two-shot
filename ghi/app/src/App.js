import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoesList from './ShoesList'
import ShoesForm from './ShoesForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatList hats={props.hats} />} />
          <Route path="hats/new" element={<HatForm />} />
          <Route path="shoes" element={<ShoesList shoes={props.shoes}/>} />
          <Route path="shoes/new" element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
