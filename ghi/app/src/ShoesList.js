import React from "react"
import { Link } from "react-router-dom"


export default function ShoesList(props) {

    const deleteShoe = async (id) => {
        const shoeUrl = `http://localhost:8080/api/shoes/${id}/`
        const fetchConfig = {
            method: "delete",
        }
    await fetch(shoeUrl, fetchConfig)
    document.location.reload();
    }

    return (

    <>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link
            to="/shoes/new"
            className="btn btn-outline-primary btn-lg px-4 gap-3"
            >
                Create a new shoe
            </Link>
        </div>
            <div className="container overflow-hidden">
                {props.shoes.map(shoe => {
                return (
                    <div className="row gy-5">
                        <div className="col">
                            <div className="card mb-3 shadow custom-card">
                                <img src={shoe.picture_url} className="card-img-top img-fluid" />
                                <div className="card-body" style={{ padding: "30px" }}>
                                        <table className="table ">
                                            <thead className="table-dark">
                                                <tr>
                                                    <th>Manufacturer</th>
                                                    <th>Name</th>
                                                    <th>Color</th>
                                                    <th>Bin</th>
                                                    <th>Closet Name</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr key={shoe.id}>
                                                    <td> { shoe.manufacturer } </td>
                                                    <td>{ shoe.name }</td>
                                                    <td>{ shoe.color }</td>
                                                    <td>{ shoe.bin.bin_number } </td>
                                                    <td>{ shoe.bin.closet_name } </td>
                                                    <td><button onClick={() => deleteShoe(shoe.id)} type="button" className="btn btn-outline-danger">Delete</button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            })}
            </div>
    </>
    )}
