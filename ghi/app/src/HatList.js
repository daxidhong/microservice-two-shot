import React from "react"
import { Link } from "react-router-dom"


export default function HatList(props) {
  const handleDelete = async (id) => {
    const deleteHat =  `http://localhost:8090/api/hats/${id}/`
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
  };
  const response = await fetch(deleteHat, fetchConfig);
  if (response.ok) {
    alert("You deleted the hat.");
  }
};
    return (
      <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>Location</th>
            <th>Closet Name</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {props.hats?.map(hat => {
            return (
              <tr key={hat.href}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location.section_number }</td>
                <td>{ hat.location.closet_name}</td>
                <td> <button onClick={() => handleDelete(hat.id)}>Delete</button> </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/hats/new" className="btn btn-outline-primary btn-lg px-4 gap-3">
                Create a hat!
            </Link>
        </div>
      </>
    );
  }
