# Wardrobify

Team:

* Person 1 (David Hong) - hats microservice
* Person 2 (Chad Manuel) - shoes microservice

## Design

## Shoes microservice

Two new models have been created., one being the Shoes model which holds our manufacturer,name, color, and picture url information. It also included the bin information which would use another model we made, BinVO, which is the value object for our bin model inside of the wardrobe microservice.

## Hats microservice


I created a Hat model that has properties that define it, which also has a foreign key that connects to another model I made, LocationVO. The purpose of LocationVO is so that we can select a hat's location whenever we make a new one.
